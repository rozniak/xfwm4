# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# 8b03701f910565d01acaec9ce3f558ab_e9f0fee <b5b06e8f85fb8288fe43ad0bc2422d94_32848>, 2013
# Arnold Marko <arnold.marko@gmail.com>, 2019-2021,2023
# Dražen M. <drazen@ubuntu.si>, 2015
# Kernc, 2015-2016
# Kernc, 2020
# Martin Srebotnjak <miles@filmsi.net>, 2022
msgid ""
msgstr ""
"Project-Id-Version: Xfwm4\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-05-30 00:57+0200\n"
"PO-Revision-Date: 2013-07-02 20:22+0000\n"
"Last-Translator: Arnold Marko <arnold.marko@gmail.com>, 2019-2021,2023\n"
"Language-Team: Slovenian (http://app.transifex.com/xfce/xfwm4/language/sl/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: sl\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);\n"

#: ../helper-dialog/helper-dialog.c:86
msgid ""
"This window might be busy and is not responding.\n"
"Do you want to terminate the application?"
msgstr "To okno se ne odziva.\nAli želite prekiniti ta program?"

#: ../helper-dialog/helper-dialog.c:91
msgid "Warning"
msgstr "Opozorilo"

#: ../settings-dialogs/tweaks-settings.c:55
msgid "None"
msgstr "Brez"

#: ../settings-dialogs/tweaks-settings.c:500
msgid "Session manager socket"
msgstr "Vtičnik upravljalnika seje"

#: ../settings-dialogs/tweaks-settings.c:500
#: ../settings-dialogs/workspace-settings.c:422
#: ../settings-dialogs/xfwm4-settings.c:240
msgid "SOCKET ID"
msgstr "ID VTIČNIKA"

#: ../settings-dialogs/tweaks-settings.c:501
#: ../settings-dialogs/workspace-settings.c:423
#: ../settings-dialogs/xfwm4-settings.c:242
msgid "Version information"
msgstr "Podatki o različici"

#: ../settings-dialogs/tweaks-settings.c:517
#: ../settings-dialogs/workspace-settings.c:440
#: ../settings-dialogs/xfwm4-settings.c:869
msgid "."
msgstr "."

#: ../settings-dialogs/tweaks-settings.c:521
#: ../settings-dialogs/workspace-settings.c:444
#: ../settings-dialogs/xfwm4-settings.c:873
#, c-format
msgid ""
"%s: %s\n"
"Try %s --help to see a full list of available command line options.\n"
msgstr "%s: %s\nPoskusite %s --help za ogled celotnega seznama možnosti ukazne vrstice.\n"

#: ../settings-dialogs/workspace-settings.c:94
#: ../settings-dialogs/workspace-settings.c:109
#, c-format
msgid "Workspace %d"
msgstr "Delovna površina %d"

#: ../settings-dialogs/workspace-settings.c:307
msgid "Workspace Name"
msgstr "Ime delovne površine"

#: ../settings-dialogs/workspace-settings.c:422
#: ../settings-dialogs/xfwm4-settings.c:240
msgid "Settings manager socket"
msgstr "Vtičnik upravljalnika nastavitev"

#: ../settings-dialogs/xfce-wm-settings.desktop.in.h:1
#: ../settings-dialogs/xfwm4-dialog.glade.h:1
msgid "Window Manager"
msgstr "Upravljalnik oken"

#: ../settings-dialogs/xfce-wm-settings.desktop.in.h:2
msgid "Configure window behavior and shortcuts"
msgstr "Nastavite bljižnic in obnašanja oken"

#: ../settings-dialogs/xfce-wm-settings.desktop.in.h:3
msgid ""
"windows;management;settings;preferences;themes;styles;decorations;title "
"bar;font;buttons;menu;minimize;maximize;shade;roll up;layout;keyboard "
"shortcuts;focus;snapping;screen;workspaces;edges;corner;hide "
"content;move;resize;moving;resizing;double click;"
msgstr "okna;upravljanje;nastavitve;možnosti;lastnosti;teme;sitili;okraski;naslovna vrstica;pisava;gumbi;meni;zmanjšaj;pomanjšaj;povečaj;senca;postavitev;bližnjice;fokus;skok;zaslon;delovne površine;robovi;koti;skrij vsebino;premakni;spremeni velikost;premikanje;dvojni klik;windows;management;settings;preferences;themes;styles;decorations;title bar;font;buttons;menu;minimize;maximize;shade;roll up;layout;keyboard shortcuts;focus;snapping;screen;workspaces;edges;corner;hide content;move;resize;moving;resizing;double click;"

#: ../settings-dialogs/xfce-wmtweaks-settings.desktop.in.h:1
#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:1
msgid "Window Manager Tweaks"
msgstr "Prilagoditve upravljalnika oken"

#: ../settings-dialogs/xfce-wmtweaks-settings.desktop.in.h:2
msgid "Fine-tune window behaviour and effects"
msgstr "Natančna nastavitev obnašanja oken in učinkov"

#: ../settings-dialogs/xfce-wmtweaks-settings.desktop.in.h:3
msgid ""
"windows;behavior;settings;preferences;cycling;cycle;switching;focus;raises;accessibility;key;dragging;move;moving;hide;frame;title"
" bar;maximized;tile;screen;edge;hot corner;snapping;mouse wheel;roll "
"up;workspaces;placement;compositor;compositing;enable;disable;shadows;decorations;opacity;resize;inactive;preview;compiz;transitions;"
msgstr "okna;obnašanja;nastavitve;lastnosti;kroženje;kroži;preklop;fokus;dvigni;dostopnost;tipka;vlecenje;premakni;premik;skrij;okvir;naslovna vrstica;povečaj;razdeli;zaslon;rob;aktivni kot;miškino kolesce;delovni prostor;postavitev;kompozitor;kompozicija;vklop;izklop;sence;okraski;prosojnost;velikost;nekativno;predogled;compiz;prehod;"

#: ../settings-dialogs/xfce-workspaces-settings.desktop.in.h:1
#: ../settings-dialogs/xfwm4-workspace-dialog.glade.h:1
msgid "Workspaces"
msgstr "Delovne površine"

#: ../settings-dialogs/xfce-workspaces-settings.desktop.in.h:2
msgid "Configure layout, names and margins"
msgstr "Nastavitev razporeda, imen in odmikov"

#: ../settings-dialogs/xfce-workspaces-settings.desktop.in.h:3
msgid ""
"workspaces;settings;preferences;virtual "
"desktops;number;windows;screen;margins;"
msgstr "delovne površine;nastavitve;lastnosti;virtualna namizja;število;okna;zasloni;robovi;"

#: ../settings-dialogs/xfwm4-dialog.glade.h:2
#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:2
#: ../settings-dialogs/xfwm4-workspace-dialog.glade.h:8
msgid "_Help"
msgstr "_Pomoč"

#. ----------------------------------------------
#: ../settings-dialogs/xfwm4-dialog.glade.h:3
#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:3
#: ../settings-dialogs/xfwm4-workspace-dialog.glade.h:9 ../src/menu.c:73
msgid "_Close"
msgstr "_Zapri"

#: ../settings-dialogs/xfwm4-dialog.glade.h:4
msgid "<b>The_me</b>"
msgstr "<b>Te_ma</b>"

#: ../settings-dialogs/xfwm4-dialog.glade.h:5
msgid "<b>Title fon_t</b>"
msgstr "<b>Pisava _naslova</b>"

#: ../settings-dialogs/xfwm4-dialog.glade.h:6
msgid "<b>Title _alignment</b>"
msgstr "<b>Por_avnava naslova</b>"

#: ../settings-dialogs/xfwm4-dialog.glade.h:7
msgid "Click and drag the buttons to change the layout"
msgstr "Kliknite in povlecite gumbe, da spremenite razpored"

#: ../settings-dialogs/xfwm4-dialog.glade.h:8
msgid "Title"
msgstr "Naslov"

#: ../settings-dialogs/xfwm4-dialog.glade.h:9
msgid "The window title cannot be removed"
msgstr "Naslova okna ni mogoče odstraniti"

#: ../settings-dialogs/xfwm4-dialog.glade.h:10
msgid "Active"
msgstr "Dejaven"

#: ../settings-dialogs/xfwm4-dialog.glade.h:11
msgid "Menu"
msgstr "Meni"

#: ../settings-dialogs/xfwm4-dialog.glade.h:12
msgid "Stick"
msgstr "Samolepilno"

#: ../settings-dialogs/xfwm4-dialog.glade.h:13
msgid "Shade"
msgstr "Osenči"

#: ../settings-dialogs/xfwm4-dialog.glade.h:14
msgid "Minimize"
msgstr "Zmanjša"

#: ../settings-dialogs/xfwm4-dialog.glade.h:15
msgid "Maximize"
msgstr "Povečaj"

#: ../settings-dialogs/xfwm4-dialog.glade.h:16
msgid "Close"
msgstr "Zapri"

#: ../settings-dialogs/xfwm4-dialog.glade.h:17
msgid "Hidden"
msgstr "Skrito"

#: ../settings-dialogs/xfwm4-dialog.glade.h:18
msgid "<b>Button layout</b>"
msgstr "<b>Razporeditev gumbov</b>"

#: ../settings-dialogs/xfwm4-dialog.glade.h:19
msgid "_Style"
msgstr "_Slog"

#: ../settings-dialogs/xfwm4-dialog.glade.h:20
msgid "Define shortcuts to perform _window manager actions:"
msgstr "Določite bližnjice za upravljanje oken:"

#: ../settings-dialogs/xfwm4-dialog.glade.h:21
msgid "_Edit"
msgstr "Ur_edi"

#: ../settings-dialogs/xfwm4-dialog.glade.h:22
msgid "_Clear"
msgstr "_Počisti"

#: ../settings-dialogs/xfwm4-dialog.glade.h:23
msgid "_Reset to Defaults"
msgstr "Ponastavi na p_rivzete vrednosti"

#: ../settings-dialogs/xfwm4-dialog.glade.h:24
msgid "_Keyboard"
msgstr "Tip_kovnica"

#: ../settings-dialogs/xfwm4-dialog.glade.h:25
msgid "Click to foc_us"
msgstr "Klikni za fo_kusiranje"

#: ../settings-dialogs/xfwm4-dialog.glade.h:26
msgid "Focus follows _mouse"
msgstr "Fokus sledi _miškinemu kazalcu"

#: ../settings-dialogs/xfwm4-dialog.glade.h:27
msgid "_Delay before window receives focus:"
msgstr "_Zakasnitev pred fokusiranjem okna:"

#. Raise focus delay
#: ../settings-dialogs/xfwm4-dialog.glade.h:29
msgid "<i>Short</i>"
msgstr "<i>kratka</i>"

#. Raise focus delay
#: ../settings-dialogs/xfwm4-dialog.glade.h:31
msgid "<i>Long</i>"
msgstr "<i>dolga</i>"

#: ../settings-dialogs/xfwm4-dialog.glade.h:32
msgid "<b>Focus model</b>"
msgstr "<b>Model fokusiranja</b>"

#: ../settings-dialogs/xfwm4-dialog.glade.h:33
msgid "Automatically give focus to _newly created windows"
msgstr "Samodejno fokusiraj na _nova okna"

#: ../settings-dialogs/xfwm4-dialog.glade.h:34
msgid "<b>New window focus</b>"
msgstr "<b>Fokusiranje novih oken</b>"

#: ../settings-dialogs/xfwm4-dialog.glade.h:35
msgid "Automatically _raise windows when they receive focus"
msgstr "Samodejni dvig fokusiranih oken"

#: ../settings-dialogs/xfwm4-dialog.glade.h:36
msgid "Delay _before raising focused window:"
msgstr "Za_kasnitev pred dvigom fokusiranega okna"

#: ../settings-dialogs/xfwm4-dialog.glade.h:37
msgid "<b>Raise on focus</b>"
msgstr "<b>Dvigni, ko je fokusiran</b>"

#: ../settings-dialogs/xfwm4-dialog.glade.h:38
msgid "Raise window when clicking _inside application window"
msgstr "Dv_ig okna ob kliku v okno programa"

#: ../settings-dialogs/xfwm4-dialog.glade.h:39
msgid "<b>Raise on click</b>"
msgstr "<b>Dvig ob kliku</b>"

#: ../settings-dialogs/xfwm4-dialog.glade.h:40
#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:19
msgid "_Focus"
msgstr "Fokusiranje"

#: ../settings-dialogs/xfwm4-dialog.glade.h:41
msgid "To screen _borders"
msgstr "k obrobam _zaslona"

#: ../settings-dialogs/xfwm4-dialog.glade.h:42
msgid "To other _windows"
msgstr "k ostalim _oknom"

#: ../settings-dialogs/xfwm4-dialog.glade.h:43
msgid "Dis_tance:"
msgstr "Raz_dalja:"

#. Edge resistance
#. Smart placement size
#: ../settings-dialogs/xfwm4-dialog.glade.h:45
#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:38
msgid "<i>Small</i>"
msgstr "<i>majhna</i>"

#. Edge resistance
#: ../settings-dialogs/xfwm4-dialog.glade.h:47
msgid "<i>Wide</i>"
msgstr "<i>velika</i>"

#: ../settings-dialogs/xfwm4-dialog.glade.h:48
msgid "<b>Windows snapping</b>"
msgstr "<b>pripenjanje oken</b>"

#: ../settings-dialogs/xfwm4-dialog.glade.h:49
msgid "With the mouse _pointer"
msgstr "Z miškinim _kazalcem"

#: ../settings-dialogs/xfwm4-dialog.glade.h:50
msgid "With a _dragged window"
msgstr "Z _vlečenjem okna"

#: ../settings-dialogs/xfwm4-dialog.glade.h:51
msgid "_Edge resistance:"
msgstr "Odpornost r_obov:"

#: ../settings-dialogs/xfwm4-dialog.glade.h:52
msgid "<b>Wrap workspaces when reaching the screen edge</b>"
msgstr "<b>Preklopi delovno površino, ko kazalec doseže rob zaslona</b>"

#: ../settings-dialogs/xfwm4-dialog.glade.h:53
msgid "When _moving"
msgstr "ob pre_mikanju"

#: ../settings-dialogs/xfwm4-dialog.glade.h:54
msgid "When _resizing"
msgstr "ob sp_reminjanju velikosti"

#: ../settings-dialogs/xfwm4-dialog.glade.h:55
msgid "<b>Hide content of windows</b>"
msgstr "<b>Skrij vsebino oken</b>"

#: ../settings-dialogs/xfwm4-dialog.glade.h:56
msgid "The action to perform when the title-bar is double-clicked"
msgstr "Dejanje, ki se izvede ob dvokliku na naslovno vrstica okna"

#: ../settings-dialogs/xfwm4-dialog.glade.h:57
msgid "<b>Double click _action</b>"
msgstr "<b>Dej_anje ob dvokliku</b>"

#: ../settings-dialogs/xfwm4-dialog.glade.h:58
msgid "Ad_vanced"
msgstr "_Napredno"

#: ../settings-dialogs/xfwm4-settings.c:218
msgid "Shade window"
msgstr "Osenči okno"

#: ../settings-dialogs/xfwm4-settings.c:219
msgid "Hide window"
msgstr "Skrij okno"

#: ../settings-dialogs/xfwm4-settings.c:220
msgid "Maximize window"
msgstr "Maksimalno povečaj okno"

#: ../settings-dialogs/xfwm4-settings.c:221
msgid "Fill window"
msgstr "Zapolni okno"

#: ../settings-dialogs/xfwm4-settings.c:222
msgid "Always on top"
msgstr "Vedno na vrhu"

#: ../settings-dialogs/xfwm4-settings.c:223
msgid "Nothing"
msgstr "Nič"

#: ../settings-dialogs/xfwm4-settings.c:228
msgid "Left"
msgstr "Levo"

#: ../settings-dialogs/xfwm4-settings.c:229
msgid "Center"
msgstr "Sredinsko"

#: ../settings-dialogs/xfwm4-settings.c:230
msgid "Right"
msgstr "Desno"

#: ../settings-dialogs/xfwm4-settings.c:346
msgid "Theme"
msgstr "Tema"

#: ../settings-dialogs/xfwm4-settings.c:496
msgid "Action"
msgstr "Dejanje"

#: ../settings-dialogs/xfwm4-settings.c:503
msgid "Shortcut"
msgstr "Bližnjica"

#: ../settings-dialogs/xfwm4-settings.c:898
#, c-format
msgid "Failed to initialize xfconf. Reason: %s"
msgstr "Ni bilo mogoče naložiti xfconf. Razlog: %s"

#: ../settings-dialogs/xfwm4-settings.c:909
msgid "Could not create the settings dialog."
msgstr "Nastavitvenega dialoga ni bilo mogoče ustvariti."

#: ../settings-dialogs/xfwm4-settings.c:1915
msgid ""
"This will reset all shortcuts to their default values. Do you really want to"
" do this?"
msgstr "Ali ste prepričani, da želite vse bližnjice ponastaviti na privzete vrednosti?"

#: ../settings-dialogs/xfwm4-settings.c:1917
msgid "Reset to Defaults"
msgstr "Ponastavi na privzete vrednosti"

#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:4
msgid ""
"S_kip windows that have \"skip pager\"\n"
"or \"skip taskbar\" properties set"
msgstr "Pres_koči okna, ki imajo nastavljeno lastnost\n\"skip pager\" ali \"skip taskbar\"."

#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:6
msgid "Cycle _through minimized windows in most recently used order"
msgstr "Kroženje skozi pomanjšana okna, razvrščenih po zadnjem dostopu"

#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:7
msgid "_Include hidden (i.e. iconified) windows"
msgstr "Pr_ikaži tudi skrita okna (npr. pomanjšana)"

#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:8
msgid "Cycle _through windows on all workspaces"
msgstr "Kroži m_ed okni vseh delovnih površin"

#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:9
msgid "_Draw frame around selected window while cycling"
msgstr "_Med kroženjem prikaži okvir okoli izbranega okna"

#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:10
msgid "_Raise windows while cycling"
msgstr "Med kroženjem _dvigni okno"

#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:11
msgid "Cycle through windows in a _list"
msgstr "Kroži med okn_i na seznamu"

#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:12
msgid "C_ycling"
msgstr "_Kroženje"

#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:13
msgid "Activate foc_us stealing prevention"
msgstr "Prepreči odvzem fokusa"

#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:14
msgid "Honor _standard ICCCM focus hint"
msgstr "Spoštuj standardni ICCCM namig"

#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:15
msgid "When a window raises itself:"
msgstr "Ko se okno dvigne:"

#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:16
msgid "_Bring window on current workspace"
msgstr "_Premakni okno na trenutno delovno površino"

#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:17
msgid "Switch to win_dow's workspace"
msgstr "Preklopi na delovno površino okna"

#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:18
msgid "Do _nothing"
msgstr "_Ne naredi ničesar"

#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:20
msgid "Key used to _grab and move windows:"
msgstr "Tipka, s katero se okno z_grabi in premika:"

#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:21
msgid "_Raise windows when any mouse button is pressed"
msgstr "Dvigni okno, ko je p_ritisnjen katerikoli miškin gumb"

#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:22
msgid "Hide frame of windows when ma_ximized"
msgstr "Skrij okvir maksimalno povečanih oken"

#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:23
msgid "Hide title of windows when maximized"
msgstr "Skrij naslovno vrstico maksimalno povečanih oken"

#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:24
msgid "Automatically _tile windows when moving toward the screen edge"
msgstr "Samodejno razpos_tavi okna ob premiku proti robu zaslona"

#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:25
msgid "Use _edge resistance instead of window snapping"
msgstr "Uporabi odpornost robov nam_esto pripenjanja"

#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:26
msgid "Use mouse wheel on title bar to ro_ll up the window"
msgstr "Miškin ko_lešček na naslovni vrstici zloži okno"

#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:27
msgid "Notify of _urgency by making window's decoration blink"
msgstr "Obvesti o _nujnosti z utripanjem okraskov okna"

#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:28
msgid "Keep urgent windows _blinking repeatedly"
msgstr "Nadaljuj z utripanjem n_ujnih oken"

#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:29
msgid "_Accessibility"
msgstr "_Dostopnost"

#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:30
msgid "Use the _mouse wheel on the desktop to switch workspaces"
msgstr "_Miškin kolešček na namizju zamenja delovno površino"

#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:31
msgid ""
"_Remember and recall previous workspace\n"
"when switching via keyboard shortcuts"
msgstr "Zapomni si in obnovi zadnjo delovno pov_ršino\npri preklapljanju s tipkovnico"

#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:33
msgid "Wrap workspaces depending on the actual desktop _layout"
msgstr "Prek_lapljaj delovne površine glede na dejansko postavitev"

#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:34
msgid "Wrap workspaces when _the first or the last workspace is reached"
msgstr "O_vij delovne površine; pred prvo pride zadnja in za zadnjo prva"

#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:35
msgid "_Workspaces"
msgstr "_Delovne površine"

#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:36
msgid "_Minimum size of windows to trigger smart placement:"
msgstr "Naj_manjša velikost okna za sproženje pametnega postavljanja:"

#. Smart placement size
#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:40
msgid "<i>Large</i>"
msgstr "<i>Velika</i>"

#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:41
msgid "By default, place windows:"
msgstr "Privzeto postavi okna:"

#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:42
msgid "At the c_enter of the screen"
msgstr "Na sr_edino zaslona"

#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:43
msgid "U_nder the mouse pointer"
msgstr "Pod miški_n kazalec"

#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:44
msgid "_Placement"
msgstr "_Postavitev"

#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:45
msgid "_Enable display compositing"
msgstr "Omogoči zaslonsko skladanj_e"

#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:46
msgid "Display _fullscreen overlay windows directly"
msgstr "Pokaži celozaslonska prekrivna okna direktno"

#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:47
msgid "Show windows preview in place of icons when cycling"
msgstr "Pokaži predogled oken namesto ikon pri kroženju"

#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:48
msgid "Show shadows under pop_up windows"
msgstr "Pokaži sence pod _pojavnimi okni"

#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:49
msgid "Show shadows under _dock windows"
msgstr "Pokaži sence pod okni _sidrišča"

#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:50
msgid "Show shadows under _regular windows"
msgstr "Pokaži sence pod _navadnimi okni"

#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:51
msgid "Zoom desktop with mouse wheel"
msgstr "Spremeni velikost namizja z miškinim koleščkom"

#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:52
msgid "Zoom pointer along with the desktop"
msgstr "Povečaj kazalec skupaj z namizjem"

#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:53
msgid "Opaci_ty of window decorations:"
msgstr "Motnos_t okenskih okraskov:"

#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:54
msgid "<i>Transparent</i>"
msgstr "<i>Prozorno</i>"

#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:55
msgid "<i>Opaque</i>"
msgstr "<i>Neprozorno</i>"

#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:56
msgid "Opacity of _inactive windows:"
msgstr "Motnost _nedejavnih oken:"

#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:57
msgid "Opacity of windows during _move:"
msgstr "Motnost oken med pre_mikanjem:"

#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:58
msgid "Opacity of windows during resi_ze:"
msgstr "Motnost oken med spreminjanem _velikosti:"

#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:59
msgid "Opacity of popup wi_ndows:"
msgstr "Motnost _pojavnih oken:"

#: ../settings-dialogs/xfwm4-tweaks-dialog.glade.h:60
msgid "C_ompositor"
msgstr "Skl_adanje"

#: ../settings-dialogs/xfwm4-workspace-dialog.glade.h:2
msgid "_Number of workspaces:"
msgstr "Število delov_nih površin:"

#: ../settings-dialogs/xfwm4-workspace-dialog.glade.h:3
msgid "Layout"
msgstr "Razporeditev"

#: ../settings-dialogs/xfwm4-workspace-dialog.glade.h:4
msgid "Names"
msgstr "Imena"

#: ../settings-dialogs/xfwm4-workspace-dialog.glade.h:5
msgid "_General"
msgstr "_Splošno"

#: ../settings-dialogs/xfwm4-workspace-dialog.glade.h:6
msgid ""
"Margins are areas on the edges of the screen where no window will be placed"
msgstr "Odmiki so področja ob robu zaslona, kamor okna ne bodo postavljena"

#: ../settings-dialogs/xfwm4-workspace-dialog.glade.h:7
msgid "_Margins"
msgstr "Od_miki"

#. TRANSLATORS: "(on %s)" is like "running on" the name of the other host
#: ../src/client.c:215
#, c-format
msgid "%s (on %s)"
msgstr "%s (na %s)"

#: ../src/keyboard.c:143 ../src/settings.c:160
#, c-format
msgid "Unsupported keyboard modifier '%s'"
msgstr "Nepodprt modifikator tipkovnice '%s'"

#: ../src/main.c:616
msgid "Set the compositor mode"
msgstr "Nastavi način skladanja"

#: ../src/main.c:618
msgid "Set the vblank mode"
msgstr "Določi vblank način"

#: ../src/main.c:628
msgid "Replace the existing window manager"
msgstr "Zamenjaj obstoječega upravljalnika oken"

#: ../src/main.c:630
msgid "Print version information and exit"
msgstr "Izpiši podatke o različici in končaj"

#: ../src/main.c:633
msgid "Enable debug logging"
msgstr "Vklopi beležke razhroščevalnika"

#: ../src/main.c:673
msgid "[ARGUMENTS...]"
msgstr "[ARGUMENTI ...]"

#: ../src/main.c:680
#, c-format
msgid "Type \"%s --help\" for usage."
msgstr "Vtipkajte \"%s --help\" za navodila za uporabo. "

#: ../src/menu.c:51
msgid "Ma_ximize"
msgstr "Maksimiraj"

#: ../src/menu.c:52
msgid "Unma_ximize"
msgstr "Pomanjšaj"

#: ../src/menu.c:53
msgid "Mi_nimize"
msgstr "Zmanjšaj"

#: ../src/menu.c:54
msgid "Minimize _Other Windows"
msgstr "Zmanjšaj _ostala okna"

#: ../src/menu.c:55
msgid "S_how"
msgstr "P_okaži"

#: ../src/menu.c:56
msgid "_Move"
msgstr "Pre_makni"

#: ../src/menu.c:57
msgid "_Resize"
msgstr "Sp_remeni velikost"

#. ----------------------------------------------
#: ../src/menu.c:59
msgid "Always on _Top"
msgstr "_Vedno na vrhu"

#: ../src/menu.c:60
msgid "_Same as Other Windows"
msgstr "_Enako kot ostala okna"

#: ../src/menu.c:61
msgid "Always _Below Other Windows"
msgstr "Vedno _pod ostalimi okni"

#. ----------------------------------------------
#: ../src/menu.c:63
msgid "Roll Window Up"
msgstr "Zarolaj okno gor"

#: ../src/menu.c:64
msgid "Roll Window Down"
msgstr "Zarolaj okno dol"

#: ../src/menu.c:65
msgid "_Fullscreen"
msgstr "_Celozaslonski način"

#: ../src/menu.c:66
msgid "Leave _Fullscreen"
msgstr "Zapusti _celozaslonski način"

#: ../src/menu.c:67
msgid "Context _Help"
msgstr "_Kontekstualna pomoč"

#. ----------------------------------------------
#: ../src/menu.c:69
msgid "Always on _Visible Workspace"
msgstr "Vedno na vidni _delovni površini"

#: ../src/menu.c:70
msgid "Move to Another _Workspace"
msgstr "Premakni na drugo delovno _površino"

#: ../src/menu.c:71
msgid "Move to Another Monitor"
msgstr "Premakni na drug zaslon"

#. --------------------------------------------------------
#: ../src/menu.c:76
msgid "Destroy"
msgstr "Uniči"

#: ../src/menu.c:79
msgid "_Quit"
msgstr "_Končaj"

#: ../src/menu.c:80
msgid "Restart"
msgstr "Znova zaženi"

#: ../src/menu.c:457
#, c-format
msgid "%s: GtkMenu failed to grab the pointer\n"
msgstr "%s: GtkMenu ni uspel zajeti kazalca\n"

#: ../src/terminate.c:77
#, c-format
msgid "Error reading data from child process: %s\n"
msgstr "Napaka pri branju podatkov podrejenega opravila: %s\n"

#: ../src/terminate.c:135
#, c-format
msgid "Cannot spawn helper-dialog: %s\n"
msgstr "Ni mogoče zagnati pomočnika: %s\n"
